﻿using Picker3D.Data;
using UnityEngine;
using TMPro;
using System.Collections.Generic;
using System.Collections;
using Picker3D.ObjectPooling;

namespace Picker3D.Game
{
    public class Pit : MonoBehaviour
    {
        #region Definitions

        //gameobjects
        [SerializeField] private GameObject PlatformGroundObject;
        [SerializeField] private GameObject LeftGateObject;
        [SerializeField] private GameObject RightGateObject;
        [SerializeField] private TMP_Text ObjectCountText;

        //cached objects
        private Level _level;
        private Vector3 _initialGroundPosition;
        private Vector3 _initialGroundScale;
        private List<PickObject> _pickObjectList;

        //constants
        private const float TIMER_LIMIT = 10.0f;

        //fields 
        private int _limitCount;
        private int _objectCount;
        private float _timer;
        private bool _isTimerActive;

        #endregion

        #region Unity Methods

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag(CommonConstants.PICK_OBJECT_TAG))
            {
                if(other.gameObject)
                    _pickObjectList.Add(other.gameObject.GetComponent<PickObject>());

                IncreaseNumber();

                if (!_isTimerActive)
                {
                    _isTimerActive = true;
                    StartCoroutine(StartTimerCoroutine());
                }
                else
                {
                    _timer = TIMER_LIMIT;
                }
            }
        }

        #endregion

        public void Initialize(Level level, int limitCount)
        {
            _level = level;

            _limitCount = limitCount;
            _objectCount = 0;
            _timer = TIMER_LIMIT;
            _isTimerActive = false;

            _initialGroundPosition = new Vector3(PlatformGroundObject.transform.position.x, PlatformGroundObject.transform.position.y, PlatformGroundObject.transform.position.z);
            _initialGroundScale = new Vector3(PlatformGroundObject.transform.localScale.x, PlatformGroundObject.transform.localScale.y, PlatformGroundObject.transform.localScale.z);

            _pickObjectList = new List<PickObject>();

            ObjectCountText.SetText(_objectCount.ToString() + " / " + _limitCount);
        }

        public void ResetData()
        {
            PlatformGroundObject.transform.position = _initialGroundPosition;
            PlatformGroundObject.transform.localScale = _initialGroundScale;
            PlatformGroundObject.SetActive(false);
        }

        private void IncreaseNumber()
        {
            _objectCount++;
            ObjectCountText.SetText(_objectCount.ToString() + " / " + _limitCount);
        }

        private void AnimatePlatformGroundObject()
        {
            PlatformGroundObject.SetActive(true);
            LeanTween.moveLocalY(PlatformGroundObject, -0.25f, 1.0f).setEase(LeanTweenType.easeInOutBack);
            LeanTween.scale(PlatformGroundObject, new Vector3(10, 0.5f, 10), 1.0f).setEase(LeanTweenType.linear)
                .setOnComplete(() => {
                    LeftGateObject.GetComponent<Animator>().enabled = true;
                    RightGateObject.GetComponent<Animator>().enabled = true;
                    _level.OnStartPlayerMove(true);
            });
        }

        private IEnumerator StartTimerCoroutine()
        {
            if (_timer >= 0.0f)
            {
                _timer -= 5f;
                yield return new WaitForSeconds(5f);
            }

            DespawnAllPickObjects();

        }

        private void DespawnAllPickObjects()
        {
            for (int i = 0; i < _pickObjectList.Count; i++)
            {
                PickObjectPool.Instance.ReturnToPool(_pickObjectList[i]);
            }

            if (_objectCount >= _limitCount)
            {
                AnimatePlatformGroundObject();
                _level.ChangeActivePitStop();
            }
            else
            {
                _level.OnSectionFailed();
            }
        }       
    }
}
