﻿using Picker3D.Controller;
using Picker3D.Data;
using System;
using UnityEngine;

namespace Picker3D.Game
{
    public class Level : MonoBehaviour
    {
        #region Definitions

        //gameobjects
        [Header("Points")]
        [SerializeField] private Transform PlayerSpawnPoint;
        [SerializeField] private Transform FirstPitStopPoint;
        [SerializeField] private Transform SecondPitStopPoint;
        [SerializeField] private Transform ThirdPitStopPoint;

        [Header("Pits")]
        [SerializeField] private Pit FirstPit;
        [SerializeField] private Pit SecondPit;
        [SerializeField] private Pit ThirdPit;

        //cached objects
        private GameController _gameController;
        private Transform _nextPitStopPoint;
        private LevelData _levelData;

        //fields
        private int _passedPit;

        //helpers
        public Vector3 PlayerSpawnPosition => PlayerSpawnPoint.position;
        public Vector3 NextPitStopPosition => _nextPitStopPoint.position;
        private Vector3 FirstPitStopPosition => FirstPitStopPoint.position;
        private Vector3 SecondPitStopPosition => SecondPitStopPoint.position;
        private Vector3 ThirdPitStopPosition => ThirdPitStopPoint.position;

        //events
        public Action<bool> OnStartPlayerMove;
        public Action OnSectionFailed;

        #endregion

        public void Initialize(GameController gameController, LevelData levelData)
        {
            _gameController = gameController;
            _levelData = levelData;
            _passedPit = -1;

            ChangeActivePitStop();

            FirstPit.Initialize(this, _levelData.FirstPitCount);
            SecondPit.Initialize(this, _levelData.SecondPitCount);
            ThirdPit.Initialize(this, _levelData.ThirdPitCount);

            _gameController.OnResetLevelSettings += ResetData;
        }

        public void ChangeActivePitStop()
        {
            _passedPit++;

            if (_passedPit == 0)
                _nextPitStopPoint = FirstPitStopPoint;
            else if (_passedPit == 1)
                _nextPitStopPoint = SecondPitStopPoint;
            else if (_passedPit == 2)
                _nextPitStopPoint = ThirdPitStopPoint;

            _gameController.PassSection(_passedPit);

            if (_passedPit == 3)
                _gameController.CompleteLevel();  
        }

        private void ResetData()
        {
            FirstPit.ResetData();
            SecondPit.ResetData();
            ThirdPit.ResetData();

            _gameController.OnResetLevelSettings -= ResetData;
        }
    }
}
