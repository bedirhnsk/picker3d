﻿using Picker3D.Controller;
using Picker3D.Data;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Picker3D.Game
{
    public class Ladle : MonoBehaviour
    {
        #region Definitions

        //cached objects
        private GameController _gameController;
        private List<PickObject> _pickObjectList;
        private Rigidbody _rigidbody;
        private Level _level;

        //constants
        private const float X_LIMIT = 3.6f
            ;
        //fields
        private Vector3 _speedVector;
        private float _deltaZ;
        private bool _isMovementActive;

        public float MoveSpeed;
        private Vector3 _lastFrameInput;

        //Events
        public Action OnChangeActivePitStop;

        #endregion

        #region Unity Methods

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        private void Update()
        {
            Vector3 move = Vector3.zero;
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                _lastFrameInput = Input.mousePosition;
            }
            else if (Input.GetKey(KeyCode.Mouse0))
            {
                Vector3 mousePosition = Input.mousePosition;
                move = Vector3.right * (mousePosition.x - _lastFrameInput.x) * MoveSpeed / 100f;
                _lastFrameInput = mousePosition;
            }

            Move(move);

            ControlLimits();
        }

        private void OnMouseDrag()
        {
            Vector3 mousePoint = Input.mousePosition;
            mousePoint.z = transform.position.z;
            _deltaZ = Camera.main.ScreenToWorldPoint(mousePoint).z;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag(CommonConstants.PICK_OBJECT_TAG))
            {
                _pickObjectList.Add(other.gameObject.GetComponent<PickObject>());
                other.gameObject.GetComponent<PickObject>().SetLadle(this);
            }
        }

        #endregion

        public void Initialize(GameController gameController, Level level)
        {
            _gameController = gameController;
            _level = level;
            _speedVector = new Vector3(0, 0, 4f);
            _pickObjectList = new List<PickObject>();

            transform.position = _level.PlayerSpawnPosition;
            _gameController.OnResetLevelSettings += ResetData;
            _level.OnStartPlayerMove += StartMove;

        }
        
        public void RunNextLevel(Vector3 position)
        {
            LeanTween.move(gameObject, position, 3.5f).setEase(LeanTweenType.linear)
                .setOnComplete(() => { _gameController.ShowResultUI(); });
        }

        private void Move(Vector3 move)
        {
            ControlPitStop();

            if (!_isMovementActive)
                return;
            
            transform.Translate((_speedVector * Time.fixedDeltaTime) + move);
        }

        private void ActivatePusher()
        {
            for (int i = 0; i < _pickObjectList.Count; i++)
            {
                _pickObjectList[i].Push();
            }

            _pickObjectList.Clear();
            _pickObjectList = new List<PickObject>();
        }

        private void ControlPitStop()
        {
            if (_isMovementActive && transform.position.z >= _level.NextPitStopPosition.z)
            {
                _isMovementActive = false;
                ActivatePusher();
            }
        }

        private void StartMove(bool isActive)
        {
            _isMovementActive = isActive;
        }

        private void ResetData()
        {
            _level = null;
            _speedVector = Vector3.zero;
            _pickObjectList = null;
            _isMovementActive = false;

            _gameController.OnResetLevelSettings -= ResetData;
        }

        private void ControlLimits()
        {
            if (transform.position.x < -X_LIMIT)
            {
                transform.position = new Vector3(-X_LIMIT, transform.position.y, transform.position.z);
            }
            else if (transform.position.x > X_LIMIT)
            {
                transform.position = new Vector3(X_LIMIT, transform.position.y, transform.position.z);
            }
        }
    }
}
