﻿using Picker3D.ObjectPooling;
using System.Collections;
using UnityEngine;

namespace Picker3D.Game
{
    public class PickObject : MonoBehaviour
    {
        #region Definitions

        //cached objects
        private Rigidbody _rigidbody;
        private Ladle _ladle;
        //constants
        private const float Y_LIMIT = 1.0f;

        //helpers
        private float TransformX => transform.position.x;
        private float TransformY => transform.position.y;
        private float TransformZ => transform.position.z;

        #endregion

        #region Unity Methods

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();    
        }

        private void Update()
        {
            if (TransformY > Y_LIMIT)
                transform.position = new Vector3(transform.position.x, Y_LIMIT, transform.position.z);

            if (_ladle)
            {
                if (TransformX > _ladle.transform.position.x + 0.65f)
                {
                    transform.position = new Vector3(_ladle.transform.position.x + 0.65f, transform.position.y, transform.position.z);
                }
                else if (TransformX < _ladle.transform.position.x - 0.65f)
                {
                    transform.position = new Vector3(_ladle.transform.position.x - 0.65f, transform.position.y, transform.position.z);
                }

                if (TransformZ < _ladle.transform.position.z + 0.4f)
                {
                    transform.position = new Vector3(transform.position.x, transform.position.y, _ladle.transform.position.z + 0.4f);
                }
            }
        }

        #endregion

        public void SetLadle(Ladle ladle)
        {
            _ladle = ladle;
        }

        public void Push()
        {
            _rigidbody.velocity = Vector3.zero;
            _rigidbody.AddForce(new Vector3(0, -15, 30));
            _ladle = null;
        }
    }
}
