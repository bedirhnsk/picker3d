﻿using UnityEngine;
using UnityEngine.UI;

namespace Picker3D.UI
{
    public class GameScreen : MonoBehaviour
    {
        #region Definitions

        //gameobjects
        [SerializeField] private Text CurrentLevelText;
        [SerializeField] private Text NextLevelText;
        [SerializeField] private Image Section1Image;
        [SerializeField] private Image Section2Image;
        [SerializeField] private Image Section3Image;

        //cached objects
        private UIController _uiController;

        #endregion

        public void Initialize(UIController uiController)
        {
            _uiController = uiController;

            _uiController.OnPassSection += PassSection;
        }
        
        public void SetData(int currentLevel)
        {
            CurrentLevelText.text = currentLevel.ToString();
            NextLevelText.text = (currentLevel + 1).ToString();

            Section1Image.color = Color.white;
            Section2Image.color = Color.white;
            Section3Image.color = Color.white;
        }

        private void PassSection(int sectionId)
        {
            switch (sectionId)
            {
                case 1:
                    Section1Image.color = Color.yellow;
                    break;
                case 2:
                    Section2Image.color = Color.yellow;
                    break;
                case 3:
                    Section3Image.color = Color.yellow;
                    break;
                default:
                    break;
            }
        }
    }
}
