﻿using UnityEngine;
using UnityEngine.UI;

namespace Picker3D.UI
{
    public class MenuScreen : MonoBehaviour
    {
        #region Definitions

        //gameobjects
        [SerializeField] private Button StartButton;

        //cached objects
        private UIController _uiController;

        #endregion

        #region Unity Methods
        private void Awake()
        {
            StartButton.onClick.AddListener(OnStartButtonClicked);    
        }

        #endregion

        public void Initialize(UIController uiController)
        {
            _uiController = uiController;

        }

        private void OnStartButtonClicked()
        {
            _uiController.OnStartGame();
            _uiController.OpenGameScreen();
        }
    }
}
