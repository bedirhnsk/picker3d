﻿using Picker3D.Data;
using Picker3D.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Picker3D
{
    public class ResultScreen : MonoBehaviour
    {
        #region Definitions

        //gameobjects
        [SerializeField] private Text ResultText;
        [SerializeField] private Text ButtonText;
        [SerializeField] private Button ContinueButton;

        //cached objects 
        private UIController _uiController;

        #endregion

        public void Initialize(UIController uiController)
        {
            _uiController = uiController;
        }

        public void SetData(ResultType resultType)
        {
            //result text configuration
            if (resultType == ResultType.Win)
                ResultText.text = "LEVEL COMPLETED";
            else if (resultType == ResultType.Fail)
                ResultText.text = "FAILED";

            //continue button text
            if (resultType == ResultType.Win)
                ButtonText.text = "CONTINUE";
            else if (resultType == ResultType.Fail)
                ButtonText.text = "RESTART";

            //continue button listener
            ContinueButton.onClick.RemoveAllListeners();

            if (resultType == ResultType.Win)
                ContinueButton.onClick.AddListener(OnContinueButtonClicked);
            else if (resultType == ResultType.Fail)
                ContinueButton.onClick.AddListener(OnRestartButtonClicked);
        }

        private void OnContinueButtonClicked()
        {
            _uiController.OpenMenuScreen();
        }

        private void OnRestartButtonClicked()
        {
            _uiController.OnRestartLevel();
        }
    }

}
