﻿using Newtonsoft.Json;
using Picker3D.Data;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Picker3D.Controller
{
    public class GameDataController
    {
        #region Singleton

        public static GameDataController Instance { get; } = new GameDataController();
        static GameDataController() { }
        GameDataController() { }

        #endregion

        #region Definitions

        public PlayerData PlayerData            { get; private set; }
        public LevelData CurrentLevelData       { get; private set; }
        public List<LevelData> LevelDataList    { get; private set; }

        #endregion

        public void SetGameData()
        {
            SetPlayerData();
            SetLevelData();
        }

        public void SetLevelData()
        {
            LevelDataList = new List<LevelData>
            {
                new LevelData { Id = 1, PrefabName = "Level1", FirstPitCount = 5, SecondPitCount = 7, ThirdPitCount = 10 },
                new LevelData { Id = 2, PrefabName = "Level2", FirstPitCount = 10, SecondPitCount = 10, ThirdPitCount = 15 },
                new LevelData { Id = 3, PrefabName = "Level3", FirstPitCount = 15, SecondPitCount = 15, ThirdPitCount = 18 },
                new LevelData { Id = 4, PrefabName = "Level4", FirstPitCount = 18, SecondPitCount = 20, ThirdPitCount = 20 },
                new LevelData { Id = 5, PrefabName = "Level5", FirstPitCount = 22, SecondPitCount = 24, ThirdPitCount = 25 },
                new LevelData { Id = 6, PrefabName = "Level6", FirstPitCount = 25, SecondPitCount = 25, ThirdPitCount = 27 },
                new LevelData { Id = 7, PrefabName = "Level7", FirstPitCount = 27, SecondPitCount = 29, ThirdPitCount = 30 },
                new LevelData { Id = 8, PrefabName = "Level8", FirstPitCount = 32, SecondPitCount = 32, ThirdPitCount = 35 },
                new LevelData { Id = 9, PrefabName = "Level9", FirstPitCount = 35, SecondPitCount = 35, ThirdPitCount = 37 },
                new LevelData { Id = 10, PrefabName = "Level10", FirstPitCount = 37, SecondPitCount = 37, ThirdPitCount = 40 },
                new LevelData { Id = 11, PrefabName = "Level11", FirstPitCount = 40, SecondPitCount = 40, ThirdPitCount = 45 },
            };

            for (int i = 0; i < LevelDataList.Count; i++)
            {
                if (LevelDataList[i].Id == PlayerData.LevelId)
                {
                    CurrentLevelData = LevelDataList[i];
                    break;
                }
            }

            if (PlayerData.LevelId > LevelDataList.Count)
            {
                int randomId = Random.Range(0, LevelDataList.Count);
                CurrentLevelData = LevelDataList[randomId];
            }

        }

        public void SetPlayerData()
        {
            PlayerData = GetCachedPlayerData();

            if (PlayerData == null)
                PlayerData = CreateNewPlayerData();
        }

        public void IncreaseLevel()
        {
            PlayerData.LevelId++;

            for (int i = 0; i < LevelDataList.Count; i++)
            {
                if (LevelDataList[i].Id == PlayerData.LevelId)
                {
                    CurrentLevelData = LevelDataList[i];
                    break;
                }
            }

            if (PlayerData.LevelId > LevelDataList.Count)
            {
                int randomId = Random.Range(0, LevelDataList.Count);
                CurrentLevelData = LevelDataList[randomId];
            }

            SetPlayerDataCache();
        }

        private PlayerData CreateNewPlayerData()
        {
            return new PlayerData
            {
                LevelId = 1
            };

        }

        private PlayerData GetCachedPlayerData()
        {
            string path = Application.persistentDataPath + CommonConstants.LocalUserDataFileName;
            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Read);

            StreamReader reader = new StreamReader(file);
            string jsonString = reader.ReadToEnd();
            Debug.Log(path);
            reader.Close();
            file.Close();

            return JsonConvert.DeserializeObject<PlayerData>(jsonString);
        }

        private void SetPlayerDataCache()
        {
            string path = Application.persistentDataPath + CommonConstants.LocalUserDataFileName;
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Write);

            StreamWriter writer = new StreamWriter(file);
            writer.Write(JsonConvert.SerializeObject(PlayerData));
            writer.Close();
            file.Close();
        }
    }
}
