﻿using Picker3D.Data;
using Picker3D.Game;
using Picker3D.ObjectPooling;
using Picker3D.UI;
using System;
using System.Collections;
using UnityEngine;

namespace Picker3D.Controller
{
    public class GameController : MonoBehaviour
    {
        #region Definitions

        //gameobjects
        [SerializeField] private Ladle Ladle;
        //[SerializeField] private Level Level;
        [SerializeField] private UIController UIController;

        //cached objects
        private Level _level;
        private GameObject _levelDataObject;
        private Level _oldLevel;

        //constants
        private const float LEVEL_LENGTH = 230.0f;

        //fields 
        private int _passedLevelCount;

        //events
        public Action OnResetLevelSettings;

        #endregion

        public void Initialize()
        {
            _passedLevelCount = 0;

            LoadLevel();
            LoadLevelData();

            UIController.Initialize(this);

            _level.Initialize(this, GameDataController.Instance.CurrentLevelData);
            Ladle.Initialize(this, _level);

            UIController.OnStartGame += StartGame;
            UIController.OnRestartLevel += RestartLevel;

            _level.OnSectionFailed += SectionFailed;

        }

        public void PassSection(int sectionId)
        {
            UIController.OnPassSection(sectionId);
        }

        public void CompleteLevel()
        {
            _passedLevelCount++;
            GameDataController.Instance.IncreaseLevel();

            LoadLevel();
            LoadLevelData();

            StartCoroutine(RunNextLevelCoroutine());
        }

        public void ShowResultUI()
        {
            UIController.OpenResultScreen(ResultType.Win);
            Destroy(_oldLevel.gameObject);
        }

        private void StartGame()
        {
            _level.Initialize(this, GameDataController.Instance.CurrentLevelData);
            Ladle.Initialize(this, _level);

            _level.OnStartPlayerMove(true);
        }

        private void LoadLevel()
        {
            _oldLevel = _level;

            _level = LevelPool.Instance.Get();
            _level.gameObject.SetActive(true);
            _level.transform.position = new Vector3(0, 0, _passedLevelCount * 230);

            _level.OnSectionFailed += SectionFailed;
        }

        private void LoadLevelData()
        {
            string prefabName = GameDataController.Instance.CurrentLevelData.PrefabName;

            GameObject levelDataPrefab = Resources.Load<GameObject>("Level/" + prefabName);

            _levelDataObject = Instantiate(levelDataPrefab, _level.transform);

        }

        private void RestartLevel()
        {
            OnResetLevelSettings();

            Destroy(_levelDataObject.gameObject);
            _levelDataObject = null;

            _level.Initialize(this, GameDataController.Instance.CurrentLevelData);
            Ladle.Initialize(this, _level);
            LoadLevelData();
            UIController.OpenMenuScreen();
        }

        private void SectionFailed()
        {
            UIController.OpenResultScreen(ResultType.Fail);
        }

        private IEnumerator RunNextLevelCoroutine()
        {
            yield return new WaitForSeconds(1.1f);

            Ladle.RunNextLevel(_level.PlayerSpawnPosition);
        }
    }
}
