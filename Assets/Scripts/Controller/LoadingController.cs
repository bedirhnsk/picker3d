﻿using UnityEngine;

namespace Picker3D.Controller
{
    public class LoadingController : MonoBehaviour
    {
        #region Definitions

        //gameobjects
        [SerializeField] private GameController GameController;

        #endregion

        private void Awake()
        {
            GameDataController.Instance.SetGameData();

            GameController.Initialize();
        }
    }
}
