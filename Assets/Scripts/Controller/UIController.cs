﻿using Picker3D.Controller;
using Picker3D.Data;
using System;
using UnityEngine;

namespace Picker3D.UI
{
    public class UIController : MonoBehaviour
    {
        #region Definitions

        //gameobjects
        [Header("Screens")]
        [SerializeField] private MenuScreen MenuScreen;
        [SerializeField] private GameScreen GameScreen;
        [SerializeField] private ResultScreen ResultScreen;

        //cached objects
        private GameController _gameController;
        private GameObject _activeScreen;

        //events
        public Action OnStartGame;
        public Action<int> OnPassSection;
        public Action OnRestartLevel;

        #endregion

        public void Initialize(GameController gameController)
        {
            _gameController = gameController;

            MenuScreen.Initialize(this);
            GameScreen.Initialize(this);
            ResultScreen.Initialize(this);

            _activeScreen = MenuScreen.gameObject;
        }

        public void OpenMenuScreen()
        {
            OpenScreen(ScreenType.Menu);
        }

        public void OpenGameScreen()
        {
            GameScreen.SetData(GameDataController.Instance.PlayerData.LevelId);
            OpenScreen(ScreenType.Game);
        }

        public void OpenResultScreen(ResultType resultType)
        {
            ResultScreen.SetData(resultType);
            OpenScreen(ScreenType.Result);
        }

        private void OpenScreen(ScreenType screenType)
        {
            _activeScreen.SetActive(false);

            switch (screenType)
            {
                case ScreenType.Menu:
                    _activeScreen = MenuScreen.gameObject;
                    break;
                case ScreenType.Game:
                    _activeScreen = GameScreen.gameObject;
                    break;
                case ScreenType.Result:
                    _activeScreen = ResultScreen.gameObject;
                    break;
                default:
                    break;
            }

            _activeScreen.SetActive(true);
        }
    }
}
