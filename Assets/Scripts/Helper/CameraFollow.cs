﻿using UnityEngine;

namespace Picker3D
{
    public class CameraFollow : MonoBehaviour
    {
        #region Definitions

        /// <summary>
        /// Camera will follow this object
        /// </summary>
        [SerializeField] private Transform Target;

        /// <summary>
        /// Camera transform
        /// </summary>
        private Transform _cameraTransform;

        /// <summary>
        /// Offset between camera and target
        /// </summary>        
        private Vector3 _offset;

        /// <summary>
        /// Change this value to get desired smoothness
        /// </summary>
        private float _smoothTime;

        /// <summary>
        ///  This value will change at the runtime depending on target movement. Initialize with zero vector.
        /// </summary>
        private Vector3 _velocity;

        #endregion

        #region Unity Methods

        private void Awake()
        {
            _cameraTransform = transform;
            _velocity = Vector3.zero;
            _smoothTime = 0.0f;
        }

        private void Start()
        {
            _offset = _cameraTransform.position - Target.position;
        }

        private void LateUpdate()
        {
            // update position
            Vector3 targetPosition = Target.position + _offset;

            targetPosition = new Vector3(transform.position.x, transform.position.y, targetPosition.z);

            _cameraTransform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref _velocity, _smoothTime);
        }

        #endregion
    }
}