﻿using System;

namespace Picker3D.Data
{
    public class LevelData
    {
        public int Id                   { get; set; }
        public string PrefabName        { get; set; }
        public int FirstPitCount        { get; set; }
        public int SecondPitCount       { get; set; }
        public int ThirdPitCount        { get; set; }
    }
}
