﻿using System;

namespace Picker3D.Data
{
    public enum ResultType :byte
    {
        Undefined = 0,
        Win = 1,
        Fail = 2
    }

    public enum ScreenType : byte
    {
        Undefined = 0,
        Menu = 1,
        Game = 2,
        Result = 3
    }

    public static class CommonConstants
    {
        public const string PICK_OBJECT_TAG = "PickObject";
        public const string PIT_TAG = "Pit";

        public const string LocalUserDataFileName = "/localuserdata.json";
    }
}
