﻿using System.Collections.Generic;
using UnityEngine;

namespace Picker3D.ObjectPooling
{
    public abstract class BaseObjectPooler<T> : MonoBehaviour where T : Component
    {
        [SerializeField]
        private T prefab;

        public static BaseObjectPooler<T> Instance { get; private set; }
        private Queue<T> objects = new Queue<T>();

        private void Awake()
        {
            Instance = this;
        }

        public T Get()
        {
            if (objects.Count == 0)
                AddObjects();
            return objects.Dequeue();
        }

        public void ReturnToPool(T objectToReturn)
        {
            objectToReturn.gameObject.SetActive(false);
            objects.Enqueue(objectToReturn);
        }

        private void AddObjects()
        {
            var newObject = Instantiate(prefab);
            newObject.gameObject.SetActive(false);
            objects.Enqueue(newObject);
        }
    }
}
